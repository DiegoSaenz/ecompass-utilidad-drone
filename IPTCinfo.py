# pip install iptcinfo
import iptcinfo
from PIL import Image

filename = "DJI_0158"
im = Image.open(filename)
im.verify()
print("declaracion")

# Not sure what other formats are supported, I never looked into it.
if im.format in ['jpg', 'TIFF']:
    try:
        print("if")
        iptc = iptcinfo.IPTCInfo(filename)

        image_title = iptc.data.get('object name', '') or iptc.data.get('headline', '')
        image_description = iptc.data.get('caption/abstract', '')
        image_tags = iptc.keywords

        print("el titulo es" + image_title)

    except Exception, e:
        if str(e) != "No IPTC data found.":
            raise