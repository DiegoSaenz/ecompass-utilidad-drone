# README #

El proyecto utilidad drone puede ser accedido unicamente desde el entorno de desarrollo, dada la necesidad de realizar lectura del directorio (fotos drone) y escribir un archivo xml local nombrado "datos.txt" por la misma razon no es posible ejecutar un pipeline pnline, aunque si se pueden realizar pruebas clonando el repositorio y bajo copias del script "get_lat_lon_exif_pil.py" que es el main() del proyecto.

### El repositorio es para: ###

* El repositorio es una copia documental, sujeta a los cambios. Contiene un software diseñado a medida.
* Version 0.8

### Requisitos ###

* Python 2.7.12
* pip
* gpsimage
* tqm

### Como ejecutar la aplicación ###

* pip install -U tox gpsimage pillow
* python get_lat_lon_exif_pil.py

![drone.png](https://bitbucket.org/repo/G5koAG/images/2652337948-drone.png)

![drone2.png](https://bitbucket.org/repo/G5koAG/images/918948672-drone2.png)

### Contacto ###

* Dueño del Repo o admin
* team contact (Ecompass)