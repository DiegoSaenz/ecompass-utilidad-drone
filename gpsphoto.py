import gpsphoto
# Get the data from image file and return a dictionary
data = gpsphoto.getGPSData('DJI_0158.jpg')
rawData = gpsphoto.getRawData('DJI_0158.jpg')

# Print out just GPS Data of interest
for tag in data.keys():
    print "%s: %s" % (tag, data[tag])

# Print out raw GPS Data for debugging
for tag in rawData.keys():
    print "%s: %s" % (tag, rawData[tag])