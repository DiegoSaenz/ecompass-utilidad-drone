#Se importan librerias
import datetime
from PIL import Image
from PIL.ExifTags import TAGS, GPSTAGS
import gpsimage
from tqdm import tqdm
import glob, os
from operator import itemgetter

# Coordinates Latitude & Longitude in Grados

def crear_encabezado():
    archi=open('datos.txt','w')
    encabezado = '''<?xml version="1.0" encoding="UTF-8" standalone="no" ?>

<gpx xmlns="http://www.topografix.com/GPX/1/1" xmlns:gpxx="http://www.garmin.com/xmlschemas/GpxExtensions/v3" xmlns:gpxtpx="http://www.garmin.com/xmlschemas/TrackPointExtension/v1" creator="Oregon 400t" version="1.1" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.topografix.com/GPX/1/1 http://www.topografix.com/GPX/1/1/gpx.xsd http://www.garmin.com/xmlschemas/GpxExtensions/v3 http://www.garmin.com/xmlschemas/GpxExtensionsv3.xsd http://www.garmin.com/xmlschemas/TrackPointExtension/v1 http://www.garmin.com/xmlschemas/TrackPointExtensionv1.xsd ">

	<trk>
		<name>Pol0001</name>\n'''

    archi.writelines(encabezado)
    archi.close()

def agregar_despegue():
    print()

def agregar_piedepagina():
    archi=open('datos.txt','a')
    piepagina="""	</trk>
</gpx>"""
    archi.writelines(piepagina)
    archi.close()


listoflists = []
list = [str, str, str, str, str]
var = ''

for infile in glob.glob("*.jpg"):
    file, ext = os.path.splitext(infile)
    img = gpsimage.open(infile)
    var = img.timestamp[0:4]+'-'+img.timestamp[5:7]+'T'+img.timestamp[11:13]+':'+img.timestamp[14:16]+':'+img.timestamp[17:19]+'.'+'000+0000'
#    print('------------'+ img.timestamp +'--------------')
    list = [img.lat, img.lng, img.altitude, var, img.timestamp]
#    print('fecha <<' + var + '>> \n')
    #print(list)
    listoflists.append(list)

listoflists = sorted(listoflists, key=itemgetter(4))


'''Aqui agregamos el despegue  (abajo)'''


primeralatitud = str(listoflists[0][0])
primeralongitud = str(listoflists[0][1])
primerafecha = str(listoflists[0][3])
primeraelevation = float(listoflists[0][2])
primerminuto = str(primerafecha[11:13])
primerminutonumber = int(primerminuto)
primerossec = str(primerafecha[14:20])
primerossecnumber = float(primerossec)


print('*** DESPEGUE *** primera fecha: '+ str(primerafecha) + ' y primera elevacion: ' + str(primeraelevation) + ' y primer minuto ' + str(primerminutonumber) + ' primeros segundos ' + format(primerossecnumber, '.4f') +' latitud ' + primeralatitud + ' longitud '+ primeralongitud)
# Se agrega despegue

while True:
  if primeraelevation<=0.0001:
    break
  if primerossecnumber <=0.1:
      primerossecnumber = 60
      primerminutonumber = primerminutonumber + 1
  var2 = primerafecha[0:4]+'-'+primerafecha[5:7]+'T'+primerafecha[8:10]+':'+str(primerminutonumber)+':'+format(primerossecnumber, '.3f')+'+0000'
  primeraelevation=primeraelevation-1.01
  primerossecnumber = primerossecnumber - 0.0019
  list = [str(primeralatitud), str(primeralongitud), str(primeraelevation), var2, 'ok']
  listoflists.insert(0, list)

'''Aqui agregamos el aterrrizaje  (abajo)'''

primeralatitud = str(listoflists[len(listoflists)-1][0])
primeralongitud = str(listoflists[len(listoflists)-1][1])
primerafecha = str(listoflists[len(listoflists)-1][3])
primeraelevation = float(listoflists[len(listoflists)-1][2])
primerminuto = str(primerafecha[11:13])
primerminutonumber = int(primerminuto)
primerossec = str(primerafecha[14:20])
primerossecnumber = float(primerossec)

print('*** ATERRIZAJE *** primera fecha: '+ str(primerafecha) + ' y primera elevacion: ' + str(primeraelevation) + ' y primer minuto ' + str(primerminutonumber) + ' primeros segundos ' + format(primerossecnumber, '.4f') +' latitud ' + primeralatitud + ' longitud '+ primeralongitud)

while True:
  if primeraelevation<=0.0001:
    break
  if primerossecnumber <=0.1:
      primerossecnumber = 60
      primerminutonumber = primerminutonumber + 1
  var2 = primerafecha[0:4]+'-'+primerafecha[5:7]+'T'+primerafecha[8:10]+':'+str(primerminutonumber)+':'+format(primerossecnumber, '.3f')+'+0000'
  primeraelevation=primeraelevation-1.01
  primerossecnumber = primerossecnumber + 0.0019
  list = [str(primeralatitud), str(primeralongitud), str(primeraelevation), var2, 'ok']
  listoflists.append(list)

crear_encabezado()

print("Creando Archivo XML")
#porcentaje = 0




for lista in tqdm(listoflists):
    #porcentaje = porcentaje +1
    iteracion= '''<trkpt lat="'''+str(lista[0])+'''" lon="'''+str(lista[1])+'''">
			<ele>'''+str(lista[2])+'''</ele>
			<time>'''+str(lista[3])+'''</time>
			<speed>0.44721360216395983</speed>
		</trkpt>\n'''
    #print('construyendo XML '+ str(porcentaje*100/len(listoflists)) + '%')
    archi=open('datos.txt','a')
    archi.writelines(iteracion)
    archi.close()
agregar_piedepagina()

